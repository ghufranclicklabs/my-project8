//
//  ViewController.swift
//  Email Tutorial
//
//  Created by clicklabs92 on 11/03/15.
//  Copyright (c) 2015 clicklabs92. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController, MFMailComposeViewControllerDelegate, UITextFieldDelegate, UITextViewDelegate  {
    
    //label of subject and body
    @IBOutlet weak var subject: UITextField!
    @IBOutlet weak var body: UITextView!
    
    @IBOutlet weak var sendMail: UIButton!
    //function for appear screen of compose mail
    @IBAction func sendMail(sender: AnyObject) {
        
        var picker = MFMailComposeViewController()
        picker.mailComposeDelegate = self
        picker.setSubject(subject.text)
        picker.setMessageBody(body.text, isHTML: true)
        presentViewController(picker, animated: true, completion: nil)
        
        //add attachment
        var image = UIImage(named: "a.jpeg")
        var imageData = UIImageJPEGRepresentation(image, 1.0)
        picker.addAttachmentData(imageData, mimeType: ".jpeg", fileName:     "a")
        //Display the view controller
        self.presentViewController(picker, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        subject.delegate = self
       body.delegate = self
    }
   override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MFMailComposeViewControllerDelegate
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    // UITextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    // UITextViewDelegate
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        body.text = textView.text
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

